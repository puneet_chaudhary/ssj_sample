import java.io.IOException;

public class Main_class
{
    public static void main(String[] args) throws IOException {
        ExecuteCommand e = new ExecuteCommand();
        e.makeConnection();
        e.execute();
    }

}
